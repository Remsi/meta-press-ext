zopfli_fork:
	rsync -av \
			--exclude *.git* \
			--exclude *.swp \
			--exclude *.swo \
			--exclude *LICENSE* \
			--exclude Makefile \
			--exclude *README.adoc \
			--exclude TODO.adoc \
			--exclude font \
			--exclude css/bootstrap/js \
			--exclude css/bootstrap/css/bootstrap.css \
			--exclude css/bootstrap/css/bootstrap-theme.css \
			--exclude css/bootstrap/css/bootstrap-theme.min.css \
			--exclude css/bootstrap/config.json \
			--exclude css/codemirror.css \
			--exclude html_locales/template.json \
			--exclude html_locales/black_list.json \
			--exclude img/src \
			--exclude img/Meta-Press.es_title.png \
			--exclude img/metapressv4-ralenti__.gif \
			--exclude js/update_black_list.js \
			--exclude js/month_nb/month_definitions \
			--exclude js/month_nb/reimplementations \
			--exclude js/month_nb/load_month_nb.js \
			--exclude js/month_nb/test_month_nb.mjs \
			--exclude js/month_nb/month_nb_node-10.x.js \
			--exclude js/gettext_html_auto.js/update_locales.js \
			--exclude js/deps/codemirror/codemirror.js \
			--exclude js/deps/codemirror/codemirror_mode_javascript.js \
		. /tmp/`date +%F_%X`-meta-press-ext_zopfli/
	cd /tmp && zopfli_fork --t$(`nproc` / 2) --zip --dir `date +%F_%X`-meta-press-ext_zopfli
	mv /tmp/*.zip ../

7zip:
	7z a -tzip -mx=9 \
			-xr!.git* \
			-xr!*.swp \
			-xr!*.swo \
			-xr!LICENSE \
			-xr!README.adoc \
			-x!*.gitmodules \
			-x!Makefile \
			-x!TODO.adoc \
			-x!lost_keys* \
			-x!font \
			-x!css/bootstrap/js \
			-x!css/bootstrap/css/bootstrap.css \
			-x!css/bootstrap/css/bootstrap-theme.css \
			-x!css/bootstrap/css/bootstrap-theme.min.css \
			-x!css/bootstrap/config.json \
			-x!css/codemirror.css \
			-x!html_locales/template.json \
			-x!html_locales/black_list.json \
			-x!img/src \
			-x!img/Meta-Press.es_title.png \
			-x!img/metapressv4-ralenti__.gif \
			-x!js/update_black_list.js \
			-x!js/month_nb/month_definitions \
			-x!js/month_nb/reimplementations \
			-x!js/month_nb/load_month_nb.js \
			-x!js/month_nb/test_month_nb.mjs \
			-x!js/month_nb/month_nb_node-10.x.js \
			-x!js/gettext_html_auto.js/update_locales.js \
			-x!js/deps/codemirror/codemirror.js \
			-x!js/deps/codemirror/codemirror_mode_javascript.js \
		../`date +%F_%X`-meta-press-ext_7zip .

zip:
	zip -9 \
			-x *.git* \
			-x .gitmodules \
			-x *.swp* \
			-x *.swo* \
			-x *LICENSE* \
			-x *README* \
			-x Makefile \
			-x TODO.adoc \
			-x lost_keys* \
			-x font/* \
			-x css/bootstrap/js/* \
			-x css/bootstrap/css/bootstrap.css \
			-x css/bootstrap/css/bootstrap-theme.css \
			-x css/bootstrap/css/bootstrap-theme.min.css \
			-x css/bootstrap/config.json \
			-x css/codemirror.css \
			-x html_locales/template.json \
			-x html_locales/black_list.json \
			-x img/src/* \
			-x img/Meta-Press.es_title.png \
			-x img/metapressv4-ralenti__.gif \
			-x js/update_black_list.js \
			-x js/month_nb/month_definitions/* \
			-x js/month_nb/reimplementations/* \
			-x js/month_nb/reimplementations/Python/* \
			-x js/month_nb/load_month_nb.js \
			-x js/month_nb/test_month_nb.mjs \
			-x js/month_nb/month_nb_node-10.x.js \
			-x js/gettext_html_auto.js/update_locales.js \
			-x js/deps/codemirror/codemirror.js \
			-x js/deps/codemirror/codemirror_mode_javascript.js \
		-v -r ../`date +%F_%X`-meta-press-ext_zip .


black_list_update:
	node js/update_black_list.js

rm_template:
	rm html_locales/template.json
	echo "{}" >  html_locales/template.json

mv_template:
	mv ~/Téléchargements/template.json html_locales/

update_locales:
	node js/gettext_html_auto.js/update_locales.js

new_templates: mv_template black_list_update update_locales

jsuglify:
	uglifyjs file.js > file.min.js

js-beautify:
	 js-beautify file.min.js > file.js

yui-compressor:
	yui-compressor -o file.min.js file.js

dl-compressed-dayjs-locales:
	for a in `ls /js/dayjs/locale/*.js`; do wget -O "js/dayjs/locale/`basename ${a} .js`.min.js" "https://unpkg.com/dayjs@1.8.17/locale/${a}"; done;


.PHONY: zip black_list_update mv_template update_locales
