// name    : setting_page.js
// author  : Simon Descarpentries, simon /\ acoeuro [] com
// licence : GPLv3
//
/* globals browser CodeMirror */
import * as µ from "./utils.js"
import * as mµ from "./mp_utils.js"
import * as g from "./gettext_html_auto.js/gettext_html_auto.js"

(async () => {
	'use strict'
	mµ.set_theme()
	await g.xgettext_html()
	await g.gettext_html_auto(mµ.get_wanted_locale())
	//
	// general settings
	//
	var lang_select = document.getElementById('mp_lang')
	lang_select.value =  mµ.get_stored_locale()
	lang_select.addEventListener("change", () => {
		localStorage.setItem('locale', µ.get_select_value(lang_select))
		location.reload()
	})
	var bg_select = document.getElementById('dark_background')
	bg_select.value = mµ.get_stored_theme()
	bg_select.addEventListener("change", () => {
		localStorage.setItem('dark_background', µ.get_select_value(bg_select))
		location.reload()
	})
	var live_search_reload = document.getElementById('live_search_reload')
	live_search_reload.checked = mµ.get_stored_live_search_reload()
	live_search_reload.addEventListener('change', () => {
		localStorage.setItem('live_search_reload', live_search_reload.checked && '1' || '')
	})
	var max_res_by_src = document.getElementById('max_res_by_src')
	max_res_by_src.value = mµ.get_stored_max_res_by_src()
	max_res_by_src.addEventListener('change', () => {
		localStorage.setItem('max_res_by_src', max_res_by_src.value)
	})
	var undup_results = document.getElementById('undup_results')
	undup_results.checked = mµ.get_stored_undup_results()
	undup_results.addEventListener('change', () => {
		localStorage.setItem('undup_results', undup_results.checked && '1' || '')
	})
	var load_photos = document.getElementById('load_photos')
	load_photos.checked = mµ.get_stored_load_photos()
	load_photos.addEventListener('change', () => {
		localStorage.setItem('load_photos', load_photos.checked && '1' || '')
	})
	var headline_loading = document.getElementById('headline_loading')
	headline_loading.checked = mµ.get_stored_headline_loading()
	headline_loading.addEventListener('change', () => {
		localStorage.setItem('headline_loading', headline_loading.checked && '1' || '')
	})
	var live_headline_reload = document.getElementById('live_headline_reload')
	live_headline_reload.checked = mµ.get_stored_live_headline_reload()
	live_headline_reload.addEventListener('change', () => {
		localStorage.setItem('live_headline_reload', live_headline_reload.checked && '1' || '')
	})
	var max_headline_loading = document.getElementById('max_headline_loading')
	max_headline_loading.value = mµ.get_stored_max_headline_loading()
	max_headline_loading.addEventListener('change', () => {
		localStorage.setItem('max_headline_loading', max_headline_loading.value)
	})
	var headline_page_size = document.getElementById('headline_page_size')
	headline_page_size.value = mµ.get_stored_headline_page_size()
	headline_page_size.addEventListener('change', () => {
		localStorage.setItem('headline_page_size', headline_page_size.value)
	})
	var keep_host_perm = document.getElementById('keep_host_perm')
	keep_host_perm.checked = mµ.get_stored_keep_host_perm()
	keep_host_perm.addEventListener('change', () => {
		localStorage.setItem('keep_host_perm', keep_host_perm.checked && '0' || '')
	})
	var provided_sources_json = await fetch("json/sources.json")
	provided_sources_json = await provided_sources_json.json()
	document.getElementById('request_host_perm').addEventListener('click', () => {
		mµ.request_sources_perm(provided_sources_json)
	})
	document.getElementById('drop_host_perm').addEventListener('click', mµ.drop_host_perm)
	//
	// custom_src
	//
	var provided_sources_text = await fetch("json/sources.json")
	provided_sources_text = await provided_sources_text.text()
	var custom_src_codemirror
	document.getElementById('provided_sources').textContent = provided_sources_text
	/* var provided_src_codemirror = */ CodeMirror.fromTextArea(
		document.getElementById('provided_sources'), {
			mode: {name: "javascript", json: true},
			indentWithTabs: true,
			readOnly: true,
		}
	)
	browser.storage.sync.get("custom_src").then(
		load_custom_src, err => {console.error(`Loading custom_src: ${err}`)}
	)
	function load_custom_src(stored_data){
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_src) == 'string' &&
				stored_data.custom_src) {
			document.getElementById('custom_sources').textContent = stored_data.custom_src
			// document.getElementById('reload_hint').style.display = 'inline'
		} else {
			document.getElementById('custom_sources').textContent =
				document.getElementById('default_custom_sources').textContent
		}
		custom_src_codemirror = CodeMirror.fromTextArea(
			document.getElementById('custom_sources'), {
				mode: {name: "javascript", json: true},
				indentWithTabs: true,
			}
		)
		const STATUS_CURRENT_LINE = document.getElementById('ln_nb')
		const STATUS_CURRENT_COL = document.getElementById('col_nb')
		custom_src_codemirror.on("cursorActivity", () => {
			const cursor = custom_src_codemirror.getCursor()
			STATUS_CURRENT_LINE.textContent = cursor.line + 1
			STATUS_CURRENT_COL.textContent = cursor.ch + 1
		})
	}
	document.getElementById('save_custom_sources').addEventListener("click", () => {
		custom_src_codemirror.save()
		browser.storage.sync.set({custom_src: document.getElementById('custom_sources').value})
		document.getElementById('reload_hint').style.display = 'inline'
		document.getElementById('reload_hint').style['font-weight'] = 'bold'
	})
})()
