// name		 : mp_utils.js
// author  : Simon Descarpentries, simon /\ acoeuro [] com
// licence : GPLv3
/* globals browser */
import * as µ from "./utils.js"

export function get_wanted_theme() {
	return get_stored_theme() || (µ.isDarkMode() && 'dark' || 'light')
}
export function set_theme() { if (get_wanted_theme() == 'light') set_light_mode() }
export function set_light_mode() {
	var html = document.getElementsByTagName('html')[0]
	html.style.cssText += "--background: var(--light-normal-background)"
	html.style.cssText += "--foreground: black"
	html.style.cssText += "--frame-background: var(--light-frame-background)"
	html.style.cssText += "--a-color: var(--mp-dark-turquoise)"
}
export function get_wanted_locale() {
	return get_stored_locale() || navigator.language || navigator.userLanguage
}
export const get_stored_locale = () => µ.get_stored('locale', '')
export const get_stored_theme = () => µ.get_stored('dark_background', '')
export const get_stored_live_search_reload = () => µ.get_stored('live_search_reload', '')
export const get_stored_undup_results = () => µ.get_stored('undup_results', '1')
export const get_stored_load_photos = () => µ.get_stored('load_photos', '1')
export const get_stored_max_res_by_src = () => µ.get_stored('max_res_by_src', 20)
export const get_stored_headline_loading = () => µ.get_stored('headline_loading', '1')
export const get_stored_live_headline_reload = () => µ.get_stored('live_headline_reload', '1')
export const get_stored_keep_host_perm = () => µ.get_stored('keep_host_perm', 1)
export const get_stored_max_headline_loading = () => µ.get_stored('max_headline_loading', 30)
export const get_stored_headline_page_size = () => µ.get_stored('headline_page_size', 5)
export function photo_loading_placeholder(s) {
	return s.replace(/<img([^>]*)src="([^"]*)"(:? srcset="[^"]*")?([^>]*)?>/g,
		'<img src="." $1 mp-data-img="$2" $3><a class="load_img" mp-data-img="$2">&#11015;</a>')
}
export function remove_photo_loading_handlers(s) { return s.replaceAll(/&#11015;/g, '') }
export function notifyUser(title, body) {
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) console.warn("Browser don't support desktop notification")
	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === "granted") createNotification(title, body)
	// If it's okay let's create a notification
	else if (Notification.permission !== "denied")
		// The Notification permission may only be requested from inside a short running
		// user-generated event handler.
		Notification.requestPermission().then(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === "granted") createNotification(title, body)
		})
	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}
function createNotification(title, body) {
	new Notification(title, {
		body: body,
		icon: 'img/favicon-metapress-v2.png',
		image: 'img/logo-metapress_sq.svg',
		badge: 'img/favicon-metapress-v2.png',
		actions: [{}]
	})
}
export async function drop_host_perm () {
	let perm = await browser.permissions.getAll()
	// console.log(`Host permissions to drop : ${perm.origins.join(', ')}`)
	// console.log(`API permissions to drop : ${perm.permissions.join(', ')}`)
	browser.permissions.remove({origins: perm.origins})
}
export async function check_host_perm (current_sources) {
	let perm = await browser.permissions.getAll()
	return get_current_source_hosts(current_sources).every(val => perm.origins.includes(val))
}
export function get_current_source_hosts (current_source_selection, sources_objs) {
	let source_hosts = []
	for (let s of current_source_selection) {
		source_hosts.push(`${s}/*`)
		source_hosts.push(`${µ.domain_part(sources_objs[s].search_url)}/*`)
	}
	return source_hosts
}
export function request_sources_perm (sources) {
	let perm = {origins: []}
	let dom = ''
	let hdr_dom = ''
	for (let s of Object.keys(sources)) {
		if (s == 'RSS' || s == 'ATOM')
			continue
		dom = `${s}/*`
		perm.origins.push(dom)
		let s_headline_url = sources[s]['headline_url']
		if (s_headline_url) {
			hdr_dom = `${s_headline_url}/*`
			if (dom =! hdr_dom)
				perm.origins.push(hdr_dom)
		}
	}
	// console.log(perm)
	browser.permissions.request(perm)
}
